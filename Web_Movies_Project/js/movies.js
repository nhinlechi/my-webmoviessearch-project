var imgpath = 'https://image.tmdb.org/t/p/w500';
var api = 'https://api.themoviedb.org/3/';
var apiKey = '?api_key=d0c8e56c17c37079e8afb41a4687a8d7';
var language = 'language=en-US';
var page = 1;

async function init() {
  var reqStr, response, rs;

  reqStr = `${api}movie/top_rated${apiKey}&${language}&page=${page}`;

  loading();
  response = await fetch(reqStr);
  rs = await response.json();
  fillListMovies(rs.results, 'Name');
}

async function eventSubmit(e) {
  var strSearch, type, reqStr, response, rs;
  e.preventDefault();

  strSearch = $('form input').val();
  type = $('#type-search').val();
  switch (type) {
    case 'Name':
      reqStr = `${api}search/movie/${apiKey}&query=${strSearch}`;
      break;
    case 'Actor':
      reqStr = `${api}search/person/${apiKey}&query=${strSearch}`;
      break;
  }

  loading();
  response = await fetch(reqStr);
  rs = await response.json();
  fillListMovies(rs.results, type);
}

function loading() {
  $('#list-movies').empty();
  $('#list-movies').append(`
    <div class="spinner-border text-success" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  `);
}

function fillListMovies(list, type) {
  $('#list-movies').empty();

  switch (type) {
    case 'Name':
      for (const mv of list) {
        appendMovieCard('#list-movies', mv);
      }
      break;
    case 'Actor':
      for (const actor of list) {
        for (const mv of actor.known_for) {
          appendMovieCard('#list-movies', mv);
        }
      }
      break;
  }
}

function appendMovieCard(selector, object) {
  $(selector).append(
    `
        <div class="col-md-4 mb-5">
          <div class="card bg-secondary" onclick = "loadMoviesDetail('${object.id}')">
            <img
              class="card-img-top img-fluid"
              src= "${imgpath}${object.poster_path}"
              alt="${object.title}"
            />
            <div class="card-body text-white">
              <h4 class="card-title">${object.title}</h4>
              <p class="card-text">
              </p>
              <a class="btn btn-success btn-block" href="#">View</a>
            </div>
          </div>
        </div>
`
  );
}

async function loadMoviesDetail(id) {
  var reqStr, response, movie;
  //Start
  reqStr = `${api}movie/${id}${apiKey}`;
  loading();

  response = await fetch(reqStr);
  movie = await response.json();

  fillMovieDetail(movie);
  //End
}

function fillMovieDetail(mv) {
  $('#list-movies').empty();

  //Fill movie detail and Debug:
  var genres = '',
    companies_img;
  mv.genres.forEach(element => {
    genres += element.name + ', ';
  });

  if (mv.belongs_to_collection !== null) {
    companies_img = imgpath + mv.belongs_to_collection.backdrop_path;
  } else companies_img = imgpath + mv.poster_path;

  $('#list-movies').append(`
  <!-- Page Content -->
  <div class="container text-white lead">
  
    <!-- Portfolio Item Heading -->
    <h1 class="my-4 display-4">${mv.title}</h1>
  
    <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-4">
          <img class="img-fluid" src="${imgpath}${mv.poster_path}" alt="${mv.title}">
        </div>
    
        <div class="col-md-4">
          <h3 class="mb-3">Movies Status</h3>
          <ul class = "list-group list-group-flush">
            <li class="list-group-item"><strong>Country: </strong>${mv.production_countries[0].name}.</li> 
            <li class="list-group-item"><strong>Released Year: </strong>${mv.release_date}.</li> 
            <li class="list-group-item"><strong>Runtime: </strong>${mv.runtime}min.</li> 
            <li class="list-group-item"><strong>Language: </strong>${mv.spoken_languages[0].name}.</li> 
            <li class="list-group-item"><strong>Genre: </strong>${genres}</li>
            <li class="list-group-item"><strong>imdbRating: </strong>${mv.vote_average}.</li>
            <li class="list-group-item"><strong>Production: </strong>${mv.production_companies[0].name}.</li>
            <li class="list-group-item"><a href="#">
            <img id = "logo-img" class="img-fluid" src="${imgpath}${mv.production_companies[0].logo_path}" alt="">
          </a></li>
          </ul>
        </div>
      </div>
    <!-- /.row -->

    <!--Add actor credits here-->
    <div class = "container">
        <h1 class = "display-4 text-center">Actors</h1>
        <div id="block-actor" class="carousel slide my-5" data-ride="carousel">
         
            <div class="carousel-inner">
              <div class = "carousel-item active">
                <ul class = "row" style="list-style-type:none;" id = "carousel-item-0">
                  <!--Add Here-->
                </ul>
              </div>
            </div>
            <!--Control-->
           
            <a href="#block-actor" class="carousel-control-prev" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
            </a>
            <a href="#block-actor" class="carousel-control-next" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
        
        </div>
    </div>
    


    <!-- Movies Content-->
      <div>
        <h3 class="my-4 text-center display-4">Movies Content</h3>
        <p>${mv.overview}</p>
      </div>

      <div class="row mb-4 justify-content-center">
        <div class="col-md-6">
          <a href="#">
            <img class="img-fluid" src="${companies_img}" alt="">
          </a>
        </div>
      </div>

      <div class="row mb-4 justify-content-center">
        <div class="col-md-6">
          <a href="#">
            <img class="img-fluid" src="${imgpath}${mv.backdrop_path}" alt="">
          </a>
        </div>
      </div>
    
    <!--Add Review here-->
    <div class = "review-list">
        <div class = "review-header display-4 text-center">Reviews</div>
    </div>     
    
    
  </div>
    <!-- /.row -->
  `);

  //Add Credits Actor Here:
  fillActorsCreditList(mv);

  //Add Review Movies:
  fillReviewMovie(mv);
}

async function fillActorsCreditList(mv) {
  var reqStr, response, actors;
  //Start
  reqStr = `${api}movie/${mv.id}/credits${apiKey}`;

  response = await fetch(reqStr);
  actors = await response.json();

  fillActorCredit(actors.cast);
}

function fillActorCredit(list) {
  var i = 0,
    idCarouselItem = 0;
  for (const actor of list) {
    if (i !== 0 && i % 6 === 0) {
      idCarouselItem++;
      $('.carousel-inner').append(`
        <div class = "carousel-item">
          <ul class="row" style="list-style-type:none;" id = "carousel-item-${idCarouselItem}"">
          </ul>
        </div>
      `);
    }
    if (i % 6 !== 0 && i % 6 !== 5)
      appendCard(
        `${actor.id}`,
        `${actor.profile_path}`,
        `${actor.name}`,
        idCarouselItem
      );
    else {
      //Empty col first and end to contain control icon (prev & next):
      $(`#carousel-item-${idCarouselItem}`).append(`
        <li class = "col-sm-2"></li>
      `);
    }
    i++;
  }
}

function appendCard(id, img, title, idCarouselItem) {
  $(`#carousel-item-${idCarouselItem}`).append(`
    <li class="col-sm-2">
    <div class="card bg-secondary" id="actor-card" onclick = "loadActorDetail(${id})">
      
        <img
          class="d-block img-fluid card-img-top"
          src="${imgpath}${img}"
          alt="Actor"
          id = "img-actor-card"
        />

      <div class="card-body" id = "actor-card-body">
        <div class="card-title text-center">
          ${title}
        </div>
      </div>
    </div>
  </li>
    `);
}

async function loadActorDetail(actorId) {
  var reqStr, response, actor;
  //Start
  reqStr = `${api}person/${actorId}${apiKey}`;
  loading();

  response = await fetch(reqStr);
  actor = await response.json();

  fillActorDetail(actor);
  //End
}

function fillActorDetail(actor) {
  $('#list-movies').empty();

  $('#list-movies').append(`
    <div class = "container text-white lead">
        <div class = "row py-2">
          <div class = "col-md-8" > 
          <p><strong>Birthday: </strong>${actor.birthday}</p>
          <p><strong>Biography: </strong>${actor.biography}</p>
          <p><strong>Place Of Birth: </strong>${actor.place_of_birth}</p>
          <p><strong>Known For Department: </strong>${actor.known_for_department}</p>
          </div>
          <div class = "col-md-4">
          <img id = "img-actor" class="img-fluid img-thumbnail" src="${imgpath}${actor.profile_path}" alt="${actor.name}">
          <h1 class = "display-4 text-center">${actor.name}</h1>
          </div>
        </div>

        <!--Add actor credits here-->
     
        <h1 class = "display-4 text-center">Film Relatives</h1>
        <div id="block-actor" class="carousel slide my-5" data-ride="carousel">
         
            <div class="carousel-inner">
              <div class = "carousel-item active">
                <ul class = "row" style="list-style-type:none;" id = "carousel-item-0">
                  <!--Add Here-->

                </ul>
              </div>
            </div>
            <!--Control-->
           
            <a href="#block-actor" class="carousel-control-prev" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
            </a>
            <a href="#block-actor" class="carousel-control-next" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
        
        </div>
    </div>
   
  `);

  loadActorMovieCredits(actor);
}

async function loadActorMovieCredits(actor) {
  var reqStr, response, moviecredit;
  //Start
  reqStr = `${api}person/${actor.id}/movie_credits${apiKey}`;

  response = await fetch(reqStr);
  moviecredit = await response.json();

  fillMovieCredits(moviecredit.cast);
  //End
}

function fillMovieCredits(list) {
  var i = 0,
    idCarouselItem = 0;
  for (const movie of list) {
    if (i !== 0 && i % 6 === 0) {
      idCarouselItem++;
      $('.carousel-inner').append(`
        <div class = "carousel-item">
          <ul class="row" style="list-style-type:none;" id = "carousel-item-${idCarouselItem}"">
          </ul>
        </div>
      `);
    }
    if (i % 6 !== 0 && i % 6 !== 5)
      appendCard(
        `${movie.id}`,
        `${movie.backdrop_path}`,
        `${movie.title}`,
        idCarouselItem
      );
    else {
      //Empty col first and end to contain control icon (prev & next):
      $(`#carousel-item-${idCarouselItem}`).append(`
        <li class = "col-sm-2"></li>
      `);
    }
    i++;
  }
}

//Fill movie review:
async function fillReviewMovie(mv) {
  var reqStr, response, review;
  //Start
  reqStr = `${api}movie/${mv.id}/reviews${apiKey}`;
  console.log(reqStr);
  response = await fetch(reqStr);
  review = await response.json();
  console.log(review);
  fillReview(review.results);
}

function fillReview(list) {
  for (const rev of list) {
    $('.review-list').append(`
    <div class = "review-item">
      <p><strong>${rev.author}: </strong>${rev.content}</p>
    </div>
  `);
  }
}
